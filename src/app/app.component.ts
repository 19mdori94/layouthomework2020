import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private snackBar: MatSnackBar) {}

  title = 'LayoutHomework2020';
  showSpinner = false;

  openSnackBar(message) {
    this.snackBar.open(message);
    setTimeout( () => {
      this.snackBar.dismiss();
    }, 5000);
  }

  onSubmit() {
    this.showSpinner = true;
    setTimeout( () => {
      this.showSpinner = false;
      this.openSnackBar('Congrats, you have opened a SnackBar! It will disappear in 5 seconds though.');
    }, 4000);
  }

}
